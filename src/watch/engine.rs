pub use super::fsm::Reactor;
use super::fsm::{BoxedState, CompoundState, State, Transition};

pub fn reactor() -> Reactor<Msg> {
    let initial_state = Box::new(CancellationWrapperState::new(Box::new(InitialState)));
    Reactor::new(initial_state, Msg::Start)
}

#[derive(Debug, Clone)]
pub enum Msg {
    BlueExited { success: bool },
    BlueStopConfirmed,
    Exited { success: bool },
    GreenExited { success: bool },
    GreenReady,
    Play,
    PlayGreen,
    QuitBlue,
    Ready,
    Reload,
    Signal,
    Start,
    StopBlue,
}

#[derive(Debug)]
struct CancellationWrapperState(CompoundState<Msg>);

impl CancellationWrapperState {
    fn new(state: BoxedState<Msg>) -> Self {
        Self(CompoundState::new(vec![state]))
    }
}

impl State for CancellationWrapperState {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::Signal => Transition::complete().with_action(Msg::Signal),
            _ => self
                .0
                .forward_to_substates(msg, |compound_state| Box::new(Self(compound_state))),
        }
    }
}

#[derive(Debug)]
struct InitialState;

impl State for InitialState {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::Start => Transition::to(Starting).with_action(Msg::Start),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct Starting;

impl State for Starting {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::Ready => Transition::to(Playing).with_action(Msg::Play),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct Playing;

impl State for Playing {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::Reload => Transition::to(Reloading::new()).with_action(Msg::Start),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct Reloading(CompoundState<Msg>);

impl Reloading {
    fn new() -> Self {
        Self(CompoundState::new(vec![
            Box::new(GreenStarting),
            Box::new(BluePlaying),
        ]))
    }
}

impl State for Reloading {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::BlueExited { success: true } if self.0.in_state::<GreenPlaying>() => {
                Transition::to(Playing)
            }
            Msg::GreenExited { .. } => Transition::to(Playing),
            _ => self
                .0
                .forward_to_substates(msg, |compound_state| Box::new(Self(compound_state))),
        }
    }
}

#[derive(Debug)]
struct BluePlaying;

impl State for BluePlaying {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::GreenReady => Transition::to(BlueStopping),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct BlueStopping;

impl State for BlueStopping {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::BlueStopConfirmed => Transition::to(BlueStopped)
                .with_action(Msg::PlayGreen)
                .with_action(Msg::QuitBlue),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct BlueStopped;

impl State for BlueStopped {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::BlueExited { success: true } => Transition::complete(),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct GreenStarting;

impl State for GreenStarting {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::GreenReady => Transition::to(GreenReady).with_action(Msg::StopBlue),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct GreenReady;

impl State for GreenReady {
    type Msg = Msg;

    fn next(self: Box<Self>, msg: Msg) -> Transition<Msg> {
        match msg {
            Msg::PlayGreen { .. } => Transition::to(GreenPlaying),
            _ => self.pass(),
        }
    }
}

#[derive(Debug)]
struct GreenPlaying;

impl State for GreenPlaying {
    type Msg = Msg;

    fn next(self: Box<Self>, _msg: Msg) -> Transition<Msg> {
        self.pass()
    }
}
