use anyhow::{Context, Result};
use async_channel::Sender;
use async_std::{io, prelude::*, task};
use serde::{Deserialize, Serialize};
use signal_hook_async_std::Signals;

#[derive(Debug, Serialize, Deserialize)]
pub enum ClientCommand {
    Play,
    Stop,
    Quit,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ClientEvent {
    PlaybackReady,
    StopConfirmed,
}

pub async fn write_child_event(event: ClientEvent) -> Result<()> {
    let event = serde_json::to_string(&event).context("serializing event to JSON")?;
    writeln!(io::stdout(), "{}", event)
        .await
        .context("writing client event to stdout")
}

pub fn pump_client_commands(sender: Sender<ClientCommand>) {
    let mut lines = io::BufReader::new(io::stdin()).lines();
    task::spawn(async move {
        while let Some(line) = lines.next().await {
            match line.context("reading next line").and_then(|line| {
                serde_json::from_str(&line).context("deserializing command from JSON")
            }) {
                Ok(command) => {
                    if sender.send(command).await.is_err() {
                        return;
                    }
                }
                Err(err) => {
                    log::error!("reading next command from stdin: {}", err);
                    continue;
                }
            };
        }
    });
}

pub fn pump_signals(mut signals: Signals, sender: Sender<ClientCommand>) {
    task::spawn(async move {
        while let Some(_) = signals.next().await {
            if sender.send(ClientCommand::Quit).await.is_err() {
                return;
            }
        }
    });
}
