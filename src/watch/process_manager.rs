use super::{
    client::{ClientCommand, ClientEvent},
    engine,
};
use anyhow::{ensure, Context, Result};
use async_channel::Sender;
use async_std::{io, prelude::*, task};
use std::{
    collections::HashMap,
    ffi::{OsStr, OsString},
    fmt, process,
};

#[derive(Debug, Clone)]
pub struct Command {
    program: OsString,
    args: Vec<OsString>,
    env: HashMap<String, String>,
}

impl Command {
    pub fn new<S: AsRef<OsStr>>(program: S) -> Self {
        Self {
            program: program.as_ref().to_owned(),
            args: Vec::new(),
            env: HashMap::new(),
        }
    }

    pub fn arg(mut self, arg: impl AsRef<OsStr>) -> Self {
        self.args.push(arg.as_ref().to_owned());
        self
    }

    pub fn args<I, S>(mut self, args: I) -> Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        self.args
            .extend(args.into_iter().map(|s| s.as_ref().to_owned()));
        self
    }

    pub fn env(mut self, key: String, value: String) -> Self {
        self.env.insert(key, value);
        self
    }
}

pub struct ProcessManager {
    handle: task::JoinHandle<()>,
    sender: Sender<engine::Msg>,
}

enum ProcessManagerState {
    Down,
    Up { process: Process },
    Switching { blue: Process, green: Process },
    ShuttingDown { processes: Vec<Process> },
}

impl ProcessManager {
    pub fn spawn(cmd: Command, event_sink: Sender<engine::Msg>) -> Self {
        let (sender, mut receiver) = async_channel::unbounded();
        let task_sender = sender.clone();

        let handle = task::spawn(async move {
            let mut state = ProcessManagerState::Down;

            while let Some(msg) = receiver.next().await {
                log::debug!(
                    "process manager received message {:?} in state {:?}",
                    msg,
                    state
                );
                match msg {
                    Msg::Msg(msg) => {
                        state = match Self::handle_message(&cmd, &task_sender, state, msg).await {
                            Some(state) => state,
                            None => {
                                log::debug!("process manager stopped");
                                return;
                            }
                        }
                    }

                    Msg::ProcessEvent(process_event) => {
                        let msg = Self::to_msg(&state, &process_event);
                        let ProcessEvent { process_id, data } = process_event;
                        state = match (state, data) {
                            (ProcessManagerState::Up { .. }, ProcessEventData::Exited { .. }) => {
                                ProcessManagerState::Down
                            }
                            (
                                ProcessManagerState::Switching { blue, green, .. },
                                ProcessEventData::Exited { .. },
                            ) => ProcessManagerState::Up {
                                process: if process_id == blue.id { green } else { blue },
                            },
                            (
                                ProcessManagerState::ShuttingDown { processes },
                                ProcessEventData::Exited { .. },
                            ) => {
                                let processes = processes
                                    .into_iter()
                                    .filter(|process| process.id != process_id)
                                    .collect::<Vec<_>>();
                                if processes.is_empty() {
                                    log::debug!("all processes have exited, process manager is shutting down");
                                    return;
                                }
                                ProcessManagerState::ShuttingDown { processes }
                            }
                            (state, _) => state,
                        };
                        if let Some(msg) = msg {
                            event_sink.send(msg).await.unwrap();
                        }
                    }
                }
            }
        });

        let (external_sender, mut external_receiver) = async_channel::unbounded();
        task::spawn(async move {
            while let Some(msg) = external_receiver.next().await {
                sender.send(Msg::Msg(msg)).await.unwrap();
            }
        });

        Self {
            handle,
            sender: external_sender,
        }
    }

    async fn handle_message(
        cmd: &Command,
        task_sender: &Sender<Msg>,
        state: ProcessManagerState,
        msg: engine::Msg,
    ) -> Option<ProcessManagerState> {
        Some(match (state, msg) {
            (ProcessManagerState::Down, engine::Msg::Signal) => return None,
            (ProcessManagerState::Up { process }, engine::Msg::Signal) => {
                process.send(ClientCommand::Quit).await;
                ProcessManagerState::ShuttingDown {
                    processes: vec![process],
                }
            }

            (ProcessManagerState::Switching { blue, green }, engine::Msg::Signal) => {
                blue.send(ClientCommand::Quit).await;
                green.send(ClientCommand::Quit).await;
                ProcessManagerState::ShuttingDown {
                    processes: vec![blue, green],
                }
            }

            (ProcessManagerState::Down, engine::Msg::Start) => ProcessManagerState::Up {
                process: Process::spawn(
                    cmd.clone(),
                    ProcessID::default(),
                    Self::new_forwarder(task_sender),
                ),
            },

            (ProcessManagerState::Up { process }, engine::Msg::Start) => {
                let process_id = process.id.other();
                ProcessManagerState::Switching {
                    blue: process,
                    green: Process::spawn(
                        cmd.clone(),
                        process_id,
                        Self::new_forwarder(task_sender),
                    ),
                }
            }

            (ProcessManagerState::Up { process }, engine::Msg::Play) => {
                process.send(ClientCommand::Play).await;
                ProcessManagerState::Up { process }
            }

            (ProcessManagerState::Switching { blue, green }, engine::Msg::StopBlue) => {
                blue.send(ClientCommand::Stop).await;
                ProcessManagerState::Switching { blue, green }
            }

            (ProcessManagerState::Switching { blue, green }, engine::Msg::PlayGreen) => {
                green.send(ClientCommand::Play).await;
                ProcessManagerState::Switching { blue, green }
            }

            (ProcessManagerState::Switching { blue, green }, engine::Msg::QuitBlue) => {
                blue.send(ClientCommand::Quit).await;
                ProcessManagerState::Switching { blue, green }
            }

            (ProcessManagerState::ShuttingDown { .. }, _) => return None,

            (state, _) => state,
        })
    }

    pub fn sender(&self) -> Sender<engine::Msg> {
        self.sender.clone()
    }

    fn new_forwarder(sink: &Sender<Msg>) -> Sender<ProcessEvent> {
        let (sender, mut receiver) = async_channel::bounded(1);
        let sink = sink.clone();

        task::spawn(async move {
            while let Some(process_event) = receiver.next().await {
                sink.send(Msg::ProcessEvent(process_event)).await.unwrap();
            }
        });

        sender
    }

    fn to_msg(state: &ProcessManagerState, process_event: &ProcessEvent) -> Option<engine::Msg> {
        Some(match state {
            ProcessManagerState::Down => {
                unreachable!("process event received but no process is running")
            }
            ProcessManagerState::Up { process } => {
                assert_eq!(process.id, process_event.process_id);
                match process_event.data {
                    ProcessEventData::Client(ClientEvent::PlaybackReady) => engine::Msg::Ready,
                    ProcessEventData::Client(ClientEvent::StopConfirmed { .. }) => {
                        unimplemented!("engine::Msg::StopConfirmed")
                    }
                    ProcessEventData::Exited { success } => engine::Msg::Exited { success },
                }
            }
            ProcessManagerState::Switching { blue, .. } => {
                if process_event.process_id == blue.id {
                    match &process_event.data {
                        ProcessEventData::Client(ClientEvent::PlaybackReady) => {
                            unimplemented!("engine::Msg::BlueReady")
                        }
                        ProcessEventData::Client(ClientEvent::StopConfirmed) => {
                            engine::Msg::BlueStopConfirmed
                        }
                        ProcessEventData::Exited { success } => {
                            engine::Msg::BlueExited { success: *success }
                        }
                    }
                } else {
                    match process_event.data {
                        ProcessEventData::Client(ClientEvent::PlaybackReady) => {
                            engine::Msg::GreenReady
                        }
                        ProcessEventData::Client(ClientEvent::StopConfirmed { .. }) => {
                            unimplemented!("engine::Msg::GreenStopConfirmed")
                        }
                        ProcessEventData::Exited { success } => {
                            engine::Msg::GreenExited { success }
                        }
                    }
                }
            }
            ProcessManagerState::ShuttingDown { .. } => return None,
        })
    }

    pub async fn wait(self) {
        self.handle.await
    }
}

impl fmt::Debug for ProcessManagerState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::result::Result<(), fmt::Error> {
        f.debug_struct(match self {
            Self::Down => "Down",
            Self::Up { .. } => "Up",
            Self::Switching { .. } => "Switching",
            Self::ShuttingDown { .. } => "ShuttingDown",
        })
        .finish()
    }
}

#[derive(Debug)]
enum Msg {
    Msg(engine::Msg),
    ProcessEvent(ProcessEvent),
}

#[derive(Debug)]
struct ProcessEvent {
    process_id: ProcessID,
    data: ProcessEventData,
}

#[derive(Debug)]
enum ProcessEventData {
    Client(ClientEvent),
    Exited { success: bool },
}

struct Process {
    command_sender: Sender<ClientCommand>,
    id: ProcessID,
}

impl Process {
    fn spawn(cmd: Command, process_id: ProcessID, sender: Sender<ProcessEvent>) -> Self {
        let (command_sender, command_receiver) = async_channel::bounded(1);

        task::spawn(async move {
            match Self::run_process(cmd, process_id, command_receiver, sender).await {
                Ok(()) => {}
                Err(err) => log::error!("subprocess crashed: {}", err),
            };
        });

        Self {
            command_sender,
            id: process_id,
        }
    }

    async fn send(&self, command: ClientCommand) {
        self.command_sender.send(command).await.unwrap();
    }

    async fn run_process(
        cmd: Command,
        process_id: ProcessID,
        receiver: async_channel::Receiver<ClientCommand>,
        sender: Sender<ProcessEvent>,
    ) -> Result<()> {
        let mut child = async_process::Command::new(cmd.program)
            .args(cmd.args)
            .stdin(process::Stdio::piped())
            .stdout(process::Stdio::piped())
            .envs(cmd.env)
            //
            // ================================================
            // These are only needed for the test watch binary.
            .env("IS_CHILD", "true")
            .env("PROCESS_ID", process_id.to_string())
            // ================================================
            //
            .spawn()?;

        let stdin = child.stdin.take().unwrap();
        task::spawn(async move {
            if let Err(err) = Self::stdin_pump(receiver, stdin).await {
                log::error!("stdin pump for process failed {}: {}", process_id, err);
            }
        });

        task::spawn(async move {
            let success = match Self::monitor_process(child, process_id, &sender).await {
                Ok(()) => true,
                Err(err) => {
                    log::error!("subprocess failed: {}", err);
                    false
                }
            };

            sender
                .send(ProcessEvent {
                    process_id,
                    data: ProcessEventData::Exited { success },
                })
                .await
                .unwrap();
        });
        Ok(())
    }

    async fn monitor_process(
        mut child: async_process::Child,
        process_id: ProcessID,
        sender: &Sender<ProcessEvent>,
    ) -> Result<()> {
        let stdout = io::BufReader::new(child.stdout.take().unwrap());
        let mut lines = stdout.lines();
        while let Some(line) = lines.next().await {
            let event = line
                .context("reading line from child process stdout")
                .and_then(|line| {
                    serde_json::from_str(&line).with_context(|| {
                        format!("decoding line from child process stdout as JSON: {}", &line)
                    })
                })
                .map(|event| ProcessEvent {
                    process_id,
                    data: ProcessEventData::Client(event),
                });
            match event {
                Ok(event) => sender.send(event).await.unwrap(),
                Err(err) => log::error!("decoding line from child process: {}", err),
            }
        }

        let status = child.status().await.context("waiting for child to exit")?;
        ensure!(status.success(), "child exited with a non-zero status");
        Ok(())
    }

    async fn stdin_pump(
        mut receiver: async_channel::Receiver<ClientCommand>,
        mut stdin: async_process::ChildStdin,
    ) -> Result<()> {
        while let Some(command) = receiver.next().await {
            let mut line = serde_json::to_vec(&command).context("encoding command as JSON")?;
            line.push('\n' as u8);
            stdin
                .write_all(&line)
                .await
                .context("sending command to child process")?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum ProcessID {
    A,
    B,
}

impl fmt::Display for ProcessID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::A => write!(f, "A"),
            Self::B => write!(f, "B"),
        }
    }
}

impl Default for ProcessID {
    fn default() -> Self {
        Self::A
    }
}

impl ProcessID {
    fn other(self) -> Self {
        match self {
            Self::A => Self::B,
            Self::B => Self::A,
        }
    }
}
