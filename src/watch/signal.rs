use super::engine;
use async_channel::Sender;
use async_std::{prelude::*, task};
use signal_hook_async_std::Signals;

pub fn pump_signals(mut signals: Signals, sender: Sender<engine::Msg>) {
    task::spawn(async move {
        while let Some(_) = signals.next().await {
            if sender.send(engine::Msg::Signal).await.is_err() {
                return;
            }
        }
    });
}
