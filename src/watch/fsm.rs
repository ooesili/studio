use async_channel::{Receiver, Sender};
use async_std::prelude::*;
use std::{
    any::{Any, TypeId},
    fmt,
};

pub type BoxedState<M> = Box<dyn State<Msg = M> + Send>;

pub struct Reactor<M> {
    action_handlers: Vec<Sender<M>>,
    receiver: Receiver<M>,
    initial_msg: M,
    sender: Sender<M>,
    state: BoxedState<M>,
}

impl<M> Reactor<M>
where
    M: fmt::Debug + Clone,
{
    pub fn new(state: BoxedState<M>, initial_msg: M) -> Self {
        let (sender, receiver) = async_channel::unbounded();
        Reactor {
            action_handlers: vec![sender.clone()],
            receiver,
            initial_msg,
            sender,
            state,
        }
    }

    pub async fn run(mut self) {
        let mut state = self.state;

        self.sender.send(self.initial_msg).await.unwrap();

        while let Some(msg) = self.receiver.next().await {
            log::debug!("Reactor received message {:?} in state {:?}", msg, state);

            let transition = state.next(msg);
            for action in transition.actions {
                log::debug!("Reactor dispatching action: {:?}", action);

                for action_handler in self.action_handlers.iter().cloned() {
                    action_handler.send(action.clone()).await.unwrap();
                }
            }
            state = match transition.state {
                TransitionState::Next(next_state) => {
                    log::debug!("transitioned to: {:?}", next_state);
                    next_state
                }
                TransitionState::Complete => {
                    log::debug!("state machine completed, exiting reactor");
                    break;
                }
            };
        }
    }

    pub fn add_action_handler(&mut self, action_handler: Sender<M>) {
        self.action_handlers.push(action_handler);
    }

    pub fn sender(&self) -> Sender<M> {
        self.sender.clone()
    }
}

#[derive(Debug)]
pub struct Transition<M> {
    actions: Vec<M>,
    state: TransitionState<M>,
}

#[derive(Debug)]
enum TransitionState<M> {
    Complete,
    Next(BoxedState<M>),
}

impl<M> Transition<M> {
    pub fn with_action(mut self, action: M) -> Self {
        self.actions.push(action);
        self
    }

    pub fn complete() -> Self {
        Self {
            actions: Vec::new(),
            state: TransitionState::Complete,
        }
    }

    pub fn to(state: impl State<Msg = M> + Send + 'static) -> Self
    where
        M: fmt::Debug,
    {
        Self {
            actions: Vec::new(),
            state: TransitionState::Next(Box::new(state)),
        }
    }
}

pub trait State: Any + fmt::Debug {
    type Msg: fmt::Debug;

    fn next(self: Box<Self>, msg: Self::Msg) -> Transition<Self::Msg>;

    fn pass(self: Box<Self>) -> Transition<Self::Msg>
    where
        Self: 'static + Sized + Send,
    {
        Transition {
            actions: Vec::new(),
            state: TransitionState::Next(self),
        }
    }
}

impl<M> dyn State<Msg = M> + Send
where
    M: 'static,
{
    pub fn is_type<T: Any>(&self) -> bool {
        self.type_id() == TypeId::of::<T>()
    }
}

#[derive(Debug)]
pub struct CompoundState<M> {
    substates: Vec<BoxedState<M>>,
}

impl<M> CompoundState<M>
where
    M: fmt::Debug + Clone + 'static,
{
    pub fn new(substates: Vec<BoxedState<M>>) -> Self {
        Self { substates }
    }

    pub fn in_state<T: Any>(&self) -> bool {
        self.substates
            .iter()
            .any(|substate| substate.is_type::<T>())
    }

    pub fn forward_to_substates(
        self,
        msg: M,
        wrap: impl FnOnce(Self) -> BoxedState<M>,
    ) -> Transition<M> {
        let mut next_states = Vec::new();
        let mut actions = Vec::new();

        for substate in self.substates {
            let mut transition = substate.next(msg.clone());
            actions.append(&mut transition.actions);
            match transition.state {
                TransitionState::Next(state) => next_states.push(state),
                TransitionState::Complete => {}
            }
        }

        Transition {
            actions,
            state: if next_states.is_empty() {
                TransitionState::Complete
            } else {
                TransitionState::Next(wrap(Self::new(next_states)))
            },
        }
    }
}
