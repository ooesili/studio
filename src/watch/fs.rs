use super::engine;
use anyhow::Context;
use async_channel::Sender;
use async_std::task;
use std::thread;

pub fn pump_notifications(args: watchexec::cli::Args, sender: Sender<engine::Msg>) {
    let notify_handler = NotifyHandler::new(args, sender);
    thread::spawn(move || {
        if let Err(err) = watchexec::watch(&notify_handler).context("") {
            log::error!("watching filesystem: {}", err);
        }
    });
}

struct NotifyHandler {
    args: watchexec::cli::Args,
    sender: Sender<engine::Msg>,
}

impl NotifyHandler {
    fn new(args: watchexec::cli::Args, sender: Sender<engine::Msg>) -> Self {
        Self { args, sender }
    }
}

impl watchexec::run::Handler for NotifyHandler {
    fn on_manual(&self) -> watchexec::error::Result<bool> {
        Ok(true)
    }

    fn on_update(&self, ops: &[watchexec::pathop::PathOp]) -> watchexec::error::Result<bool> {
        log::debug!("filesystem changes detected: {:?}", ops);
        task::block_on(async move {
            self.sender.send(engine::Msg::Reload).await.unwrap();
        });
        Ok(true)
    }

    fn args(&self) -> watchexec::cli::Args {
        self.args.clone()
    }
}
