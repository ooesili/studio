use crate::app::TimedEvent;
use crate::idpool::IDPool;
use crate::project::Track;
use anyhow::{anyhow, Result};
use sorceress::{
    pattern::Pattern,
    server::{self, Command, Server},
    synthdef::SynthDef,
    ugen,
};
use std::cell::RefCell;
use std::collections::HashMap;
use std::iter;
use std::path::PathBuf;
use std::time::Duration;

const BUFFER_SIZE: i32 = 131072;

#[derive(Debug)]
pub struct Tracker<E> {
    tracks: Vec<Track<E>>,
}

impl<E> Tracker<E> {
    pub fn new() -> Self {
        Self { tracks: Vec::new() }
    }

    pub fn load(
        self,
        jack_latency: Duration,
        region_directory: PathBuf,
        server: Server,
        buffer_ids: IDPool,
        synth_ids: IDPool,
    ) -> Result<LoadedTracker<E>> {
        let mut track_buffer_numbers = HashMap::new();

        let track_infos = self
            .tracks
            .iter()
            .map(|track| {
                (
                    track.name.clone(),
                    TrackInfo {
                        channels: track.channels,
                        record: track.record,
                    },
                )
            })
            .collect::<HashMap<_, _>>();

        for (track_name, track_info) in track_infos.iter() {
            if track_info.record {
                let buffer_number = buffer_ids.next();
                let take = 1; // TODO: make this unique
                let file_path = region_directory
                    .join(format!("{}-{}.wav", track_name, take))
                    .to_str()
                    .ok_or_else(|| anyhow!("track name is not valid UTF-8"))?
                    .to_owned();
                server.send_sync(
                    server::BufferAllocate::new(buffer_number, BUFFER_SIZE)
                        .number_of_channels(track_info.channels.count()),
                )?;
                track_buffer_numbers.insert(track_name.clone(), buffer_number);
                server.send_sync(
                    server::BufferWrite::new(
                        buffer_number,
                        file_path,
                        server::HeaderFormat::Wav,
                        server::SampleFormat::Int24,
                    )
                    .number_of_frames(0)
                    .leave_file_open(),
                )?;
            } else {
                let buffer_number = buffer_ids.next();
                server.send_sync(
                    server::BufferAllocate::new(buffer_number, BUFFER_SIZE)
                        .number_of_channels(track_info.channels.count()),
                )?;
                track_buffer_numbers.insert(track_name.clone(), buffer_number);
                let file_path =
                    // TODO: don't hardcode this
                    "/home/ooesili/.local/share/sorceress/regions/guitar-left-1.wav".to_owned();
                server.send_sync(
                    server::BufferRead::new(buffer_number, file_path)
                        .number_of_frames(BUFFER_SIZE)
                        .leave_file_open(),
                )?;
            }
        }

        let state = RefCell::new(TrackerState {
            track_buffer_numbers,
            track_recording_synth_ids: HashMap::new(),
        });

        Ok(LoadedTracker {
            jack_latency,
            pattern: Track::pattern(self.tracks),
            server,
            state,
            synth_ids,
            track_infos,
        })
    }

    pub fn add_track(&mut self, track: Track<E>) {
        self.tracks.push(track);
    }

    // TODO: move me to LoadedTracker
    pub fn synthdefs(&self) -> Vec<SynthDef> {
        let recording_mono = SynthDef::new("recording_mono", |params| {
            let buffer_number = params.named("bufnum", 0.0);
            let input = params.named("input", 0.0);
            ugen::DiskOut::ar().bufnum(buffer_number).channels(input)
        });
        let recording_stereo = SynthDef::new("recording_stereo", |params| {
            let buffer_number = params.named("bufnum", 0.0);
            let left = params.named("left", 0.0);
            let right = params.named("right", 0.0);
            ugen::DiskOut::ar()
                .bufnum(buffer_number)
                .channels(ugen::SoundIn::ar().bus(vec![left, right]))
        });
        let playback_mono = SynthDef::new("playback_mono", |params| {
            let buffer_number = params.named("bufnum", 0.0);
            let number_of_channels = 1;
            ugen::OffsetOut::ar().channels(ugen::DiskIn::ar(number_of_channels, buffer_number))
        });
        let playback_stereo = SynthDef::new("playback_stereo", |params| {
            let buffer_number = params.named("bufnum", 0.0);
            let number_of_channels = 2;
            ugen::OffsetOut::ar().channels(ugen::DiskIn::ar(number_of_channels, buffer_number))
        });

        vec![
            recording_mono,
            recording_stereo,
            playback_mono,
            playback_stereo,
        ]
    }
}

#[derive(Debug)]
struct TrackerState {
    track_buffer_numbers: HashMap<String, i32>,
    track_recording_synth_ids: HashMap<String, i32>,
}

pub struct LoadedTracker<E> {
    jack_latency: Duration,
    pattern: Pattern<E>,
    server: Server,
    state: RefCell<TrackerState>,
    synth_ids: IDPool,
    track_infos: HashMap<String, TrackInfo>,
}

impl<E> LoadedTracker<E>
where
    E: Clone,
{
    pub fn send(&self, event: TimedEvent<Event>) -> Result<()> {
        let mut state = self.state.borrow_mut();
        match event.data {
            Event::PlaySynth {
                args,
                synth_def_name,
            } => {
                let synth_id = self.synth_ids.next();
                let add_target_id = 0;
                let command = server::SynthNew::new(synth_def_name, add_target_id)
                    .controls(args.into_iter().map(|(name, value)| {
                        server::Control::new(name, server::ControlValue::Float(value))
                    }))
                    .synth_id(synth_id);
                self.server
                    .send(server::Bundle::new(event.time, iter::once(command)))?;
                Ok(())
            }
            Event::TrackStarted { segment } => {
                let synth_id = self.synth_ids.next();
                let track_info = self.track_infos.get(&segment.track_name).unwrap();
                let buffer_number = *state.track_buffer_numbers.get(&segment.track_name).unwrap();
                if self.is_recording(&segment)? {
                    let command = match track_info.channels {
                        Channels::Mono(channel) => {
                            server::SynthNew::new("recording_mono".to_string(), 0)
                                .controls(vec![
                                    server::Control::new(
                                        "bufnum",
                                        server::ControlValue::Int(buffer_number),
                                    ),
                                    server::Control::new(
                                        "input",
                                        server::ControlValue::Int(channel),
                                    ),
                                ])
                                .synth_id(synth_id)
                        }
                        Channels::Stereo(left, right) => {
                            server::SynthNew::new("recording_stereo".to_string(), 0)
                                .controls(vec![
                                    server::Control::new(
                                        "bufnum",
                                        server::ControlValue::Int(buffer_number),
                                    ),
                                    server::Control::new("left", server::ControlValue::Int(left)),
                                    server::Control::new("right", server::ControlValue::Int(right)),
                                ])
                                .synth_id(synth_id)
                        }
                    };
                    self.server.send(server::Bundle::new(
                        event.time + self.jack_latency,
                        iter::once(command),
                    ))?;
                    state
                        .track_recording_synth_ids
                        .insert(segment.track_name, synth_id);
                } else {
                    let command = server::SynthNew::new(
                        match track_info.channels {
                            Channels::Mono(_) => "playback_mono".to_string(),
                            Channels::Stereo(_, _) => "playback_stereo".to_string(),
                        },
                        0,
                    )
                    .controls(vec![server::Control::new(
                        "bufnum",
                        server::ControlValue::Int(buffer_number),
                    )])
                    .synth_id(synth_id);
                    self.server
                        .send(server::Bundle::new(event.time, iter::once(command)))?;
                }
                Ok(())
            }
            Event::TrackStopped { segment } => {
                if self.is_recording(&segment)? {
                    let synth_id = *state
                        .track_recording_synth_ids
                        .get(&segment.track_name)
                        .unwrap_or_else(|| {
                            panic!(
                                "{:?}[{:?}]",
                                state.track_recording_synth_ids, &segment.track_name
                            )
                        });

                    let buffer_number = *state
                        .track_buffer_numbers
                        .get(&segment.track_name)
                        .expect("TODO: make this impossible");
                    let close_recording_buffer = server::BufferClose::new(buffer_number);
                    let free_recording_synth = server::NodeFree::new(vec![synth_id]);
                    self.server.send(server::Bundle::new(
                        event.time + self.jack_latency,
                        vec![
                            free_recording_synth.into_packet(),
                            close_recording_buffer.into_packet(),
                        ],
                    ))?;
                    state.track_recording_synth_ids.remove(&segment.track_name);
                } else {
                    // TODO: kill synth
                }
                Ok(())
            }
        }
    }

    fn is_recording(&self, segment: &Segment) -> Result<bool> {
        let track = self
            .track_infos
            .get(&segment.track_name)
            .ok_or_else(|| anyhow!("unknown track name {}", segment.track_name))?;
        // TODO: allow segments to be specified for recording
        Ok(track.record)
    }

    pub fn shutdown(self) -> Result<()> {
        // TODO: remember buffers that weren't freed by TrackStopped events and free them
        // later at some period
        for buffer_number in self
            .state
            .borrow_mut()
            .track_buffer_numbers
            .values()
            .copied()
        {
            // TODO: free buffer numbers
            self.server
                .send(server::BufferClose::new(buffer_number))
                .err()
                .map(|err| log::error!("error closing buffer: {:?}", err));
        }
        self.server.sync()?;
        Ok(())
    }

    pub fn pattern(&self) -> Pattern<E> {
        self.pattern.clone()
    }
}

#[derive(Debug, Clone)]
pub enum Event {
    PlaySynth {
        args: Vec<(String, f32)>,
        synth_def_name: String,
    },
    TrackStarted {
        segment: Segment,
    },
    TrackStopped {
        segment: Segment,
    },
}

/// A description of a track.
#[derive(Debug)]
pub struct TrackInfo {
    pub channels: Channels,
    pub record: bool,
}

/// The number of channels and SuperCollider bus numbers for a track.
#[derive(Debug, Clone, Copy)]
pub enum Channels {
    Mono(i32),
    Stereo(i32, i32),
}

impl Channels {
    fn count(&self) -> i32 {
        match self {
            Self::Mono(_) => 1,
            Self::Stereo(_, _) => 2,
        }
    }
}

/// A named span of a track.
#[derive(Debug, Clone)]
pub struct Segment {
    pub track_name: String,
    pub section_name: String,
}
