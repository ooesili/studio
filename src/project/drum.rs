use crate::app::TimedEvent;
use crate::idpool::IDPool;
use crate::state;
use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};
use sorceress::{
    server::{self, Server},
    synthdef::{self, Input, SynthDef},
    ugen,
};
use std::collections::{HashMap, HashSet};
use std::iter;

use std::path::{Path, PathBuf};

type Slot = state::Slot<StateDTO>;

#[derive(Debug, Serialize, Deserialize)]
pub struct Kit {
    pub meta: Meta,
    pub instruments: HashMap<String, Instrument>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Meta {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Instrument {
    #[serde(default)]
    pub pan: f32,
    pub layers: Vec<Layer>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Layer {
    pub velocity: f32,
    pub samples: Vec<Sample>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Sample {
    #[serde(default = "Sample::default_number_of_frames")]
    pub number_of_frames: i32,
    pub path: PathBuf,
    #[serde(default)]
    pub starting_frame: i32,
    #[serde(default = "Sample::default_volume")]
    pub volume: f32,
}

impl Sample {
    const fn default_number_of_frames() -> i32 {
        -2
    }

    const fn default_volume() -> f32 {
        1.0
    }
}

impl Kit {
    pub fn samples(&self) -> Vec<&Sample> {
        self.instruments
            .values()
            .flat_map(|instrument| {
                instrument
                    .layers
                    .iter()
                    .flat_map(|layer| layer.samples.iter())
            })
            .collect()
    }

    pub fn set_sample_dir(&mut self, sample_dir: impl AsRef<Path>) {
        for instrument in self.instruments.values_mut() {
            for layer in instrument.layers.iter_mut() {
                for sample in layer.samples.iter_mut() {
                    sample.path = sample_dir.as_ref().join(&sample.path)
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct Machine {
    buffers: HashMap<Buffer, BufferState>,
    kits: HashMap<String, Kit>,
}

impl Machine {
    pub fn new() -> Self {
        Self {
            buffers: HashMap::new(),
            kits: HashMap::new(),
        }
    }

    pub fn load(self, server: Server, slot: Slot, buffer_ids: IDPool) -> Result<LoadedMachine> {
        let desired_buffers = Self::buffers(self.kits.values());

        let mut state_dto = slot.lock();
        let mut actual_buffers = actual_buffers_to_map(&state_dto.actual_buffers);
        statesync::add_new_buffers(&server, &buffer_ids, &mut actual_buffers, &desired_buffers)?;
        state_dto.actual_buffers = actual_buffers_from_map(&actual_buffers);
        state_dto.desired_buffers = desired_buffers.into_iter().collect();
        drop(state_dto);

        Ok(LoadedMachine {
            buffer_ids,
            buffers: actual_buffers,
            kits: self.kits,
            server,
            slot,
        })
    }

    pub fn buffers<'a>(kits: impl IntoIterator<Item = &'a Kit>) -> HashSet<Buffer> {
        kits.into_iter()
            .flat_map(Kit::samples)
            .map(|sample| Buffer {
                starting_frame: sample.starting_frame,
                number_of_frames: sample.number_of_frames,
                file_path: sample.path.clone(),
            })
            .collect()
    }

    pub fn add_kit(&mut self, kit: Kit) {
        self.kits.insert(kit.meta.name.clone(), kit);
    }
}

pub struct LoadedMachine {
    buffer_ids: IDPool,
    buffers: HashMap<Buffer, BufferState>,
    kits: HashMap<String, Kit>,
    server: Server,
    slot: Slot,
}

impl LoadedMachine {
    pub fn send(&self, event: TimedEvent<Event>) -> Result<()> {
        let (_, instrument) = self
            .kits
            .get(&event.data.kit)
            .ok_or_else(|| anyhow!("unknown kit: {}", event.data.kit))?
            .instruments
            .iter()
            .find(|(name, _)| *name == &event.data.instrument)
            .ok_or_else(|| {
                anyhow!(
                    "unknown instrument in kit {}: {}",
                    event.data.kit,
                    event.data.instrument,
                )
            })?;

        let layer = instrument
            .layers
            .iter()
            // TODO: Think this math through harder when you're more awake.
            .find(|layer| layer.velocity <= event.data.velocity)
            .ok_or_else(|| {
                anyhow!(
                    "no layer for velocity in kit {}, instrument {}: {}",
                    event.data.kit,
                    event.data.instrument,
                    event.data.velocity
                )
            })?;
        let sample_number = 0; // TODO: make me random
        let sample = &layer.samples[sample_number];
        let (_, buffer_state) = self
            .buffers
            .iter()
            .find(|(buffer, _)| {
                buffer.starting_frame == sample.starting_frame
                    && buffer.number_of_frames == sample.number_of_frames
                    && buffer.file_path == sample.path
            })
            .ok_or_else(|| {
                anyhow!(
                    "buffer not found for sample: {:?} (event: {:?})",
                    sample,
                    event.data
                )
            })?;

        let synth_def_name = match buffer_state.number_of_channels {
            1 => "drum_machine_mono".to_owned(),
            2 => "drum_machine_stereo".to_owned(),
            n => panic!("cannot play sample with {} channels", n),
        };

        self.server.send(server::Bundle::new(
            event.time,
            iter::once(server::SynthNew::new(synth_def_name, 0).controls(vec![
                server::Control::new(
                    "buffer_number",
                    server::ControlValue::Int(buffer_state.buffer_number),
                ),
                server::Control::new("pan", server::ControlValue::Float(instrument.pan)),
                server::Control::new("volume", server::ControlValue::Float(sample.volume)),
            ])),
        ))?;
        Ok(())
    }

    pub fn synthdefs(&self) -> Vec<SynthDef> {
        let mono = synthdef::SynthDef::new("drum_machine_mono", |params| {
            let buffer_number = params.named("buffer_number", 0.0);
            let pan = params.named("pan", 0.0);
            let volume = params.named("volume", 1.0);
            let buf = ugen::PlayBuf::ar(1)
                .bufnum(buffer_number)
                .done_action(synthdef::DoneAction::FreeSelf);
            let panned = ugen::Pan2::ar().input(buf).pos(pan);
            ugen::OffsetOut::ar().channels(volume.mul(panned))
        });

        let stereo = synthdef::SynthDef::new("drum_machine_stereo", |params| {
            let buffer_number = params.named("buffer_number", 0.0);
            let pan = params.named("pan", 0.0);
            let volume = params.named("volume", 1.0);
            let (left, right) = ugen::PlayBuf::ar(2)
                .bufnum(buffer_number)
                .done_action(synthdef::DoneAction::FreeSelf)
                .into_value()
                .unwrap_stereo();
            let panned = ugen::Balance2::ar().left(left).right(right).pos(pan);
            ugen::OffsetOut::ar().channels(volume.mul(panned))
        });

        vec![mono, stereo]
    }

    pub fn shutdown(self) -> Result<()> {
        let mut state_dto = self.slot.lock();
        let mut actual_buffers = actual_buffers_to_map(&state_dto.actual_buffers);
        let desired_buffers = state_dto.desired_buffers.iter().cloned().collect();
        statesync::remove_old_buffers(
            &self.server,
            &self.buffer_ids,
            &mut actual_buffers,
            &desired_buffers,
        )?;
        state_dto.actual_buffers = actual_buffers_from_map(&actual_buffers);
        state_dto.desired_buffers = desired_buffers.into_iter().collect();
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Event {
    kit: String,
    instrument: String,
    velocity: f32,
}

pub fn trigger(kit: impl Into<String>, instrument: impl Into<String>, velocity: f32) -> Event {
    Event {
        kit: kit.into(),
        instrument: instrument.into(),
        velocity,
    }
}

#[derive(Debug, Hash, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Buffer {
    pub file_path: PathBuf,
    pub starting_frame: i32,
    pub number_of_frames: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BufferState {
    pub buffer_number: i32,
    pub number_of_channels: i32,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct StateDTO {
    actual_buffers: Vec<NumberedBufferJSON>,
    desired_buffers: Vec<Buffer>,
}

fn actual_buffers_to_map(actual_buffers: &[NumberedBufferJSON]) -> HashMap<Buffer, BufferState> {
    actual_buffers
        .into_iter()
        .map(
            |NumberedBufferJSON {
                 buffer,
                 buffer_state,
             }| (buffer.clone(), buffer_state.clone()),
        )
        .collect()
}

fn actual_buffers_from_map(buffers: &HashMap<Buffer, BufferState>) -> Vec<NumberedBufferJSON> {
    buffers
        .iter()
        .map(|(buffer, buffer_state)| NumberedBufferJSON {
            buffer: buffer.clone(),
            buffer_state: buffer_state.clone(),
        })
        .collect()
}

#[derive(Debug, Serialize, Deserialize)]
struct NumberedBufferJSON {
    buffer: Buffer,
    buffer_state: BufferState,
}

mod statesync {
    use super::*;
    use sorceress::server;
    use std::collections::{HashMap, HashSet};

    #[derive(Debug)]
    pub struct DesiredState {
        pub buffers: HashSet<Buffer>,
    }

    pub fn remove_old_buffers(
        server: &Server,
        buffer_ids: &IDPool,
        actual_buffers: &mut HashMap<Buffer, BufferState>,
        desired_buffers: &HashSet<Buffer>,
    ) -> Result<()> {
        actual_buffers
            .keys()
            .cloned()
            .filter(|buffer| !desired_buffers.contains(buffer))
            .collect::<Vec<_>>()
            .into_iter()
            .map(|buffer| {
                let buffer_number = actual_buffers.remove(&buffer).unwrap().buffer_number;
                server.send(server::BufferFree::new(buffer_number))?;
                actual_buffers.remove(&buffer);
                buffer_ids.release(buffer_number);
                Ok(())
            })
            .collect::<Result<()>>()?;
        if !actual_buffers.is_empty() {
            server.sync()?;
        }
        Ok(())
    }

    pub fn add_new_buffers(
        server: &server::Server,
        buffer_ids: &IDPool,
        actual_buffers: &mut HashMap<Buffer, BufferState>,
        desired_buffers: &HashSet<Buffer>,
    ) -> Result<()> {
        let new_buffers = desired_buffers
            .iter()
            .cloned()
            .filter(|buffer| !actual_buffers.contains_key(buffer))
            .collect::<Vec<_>>();

        let new_buffer_numbers = iter::repeat_with(|| buffer_ids.next())
            .take(new_buffers.len())
            .collect::<Vec<_>>();

        for (buffer, buffer_number) in new_buffers.iter().zip(new_buffer_numbers.iter().copied()) {
            let file_path = buffer
                .file_path
                .clone()
                .into_os_string()
                .into_string()
                .unwrap();
            server.send(
                server::BufferAllocateRead::new(buffer_number, file_path)
                    .starting_frame(buffer.starting_frame)
                    .number_of_frames(buffer.number_of_frames),
            )?;
        }

        if !new_buffer_numbers.is_empty() {
            server.sync()?;
            let reply = server.send_sync(server::BufferQuery::new(new_buffer_numbers))?;
            let queried_buffers = match reply {
                server::Reply::BufferInfo { buffers } => buffers,
                _ => unreachable!(),
            };
            new_buffers
                .into_iter()
                .zip(queried_buffers)
                .for_each(|(buffer, buffer_info)| {
                    actual_buffers.insert(
                        buffer,
                        BufferState {
                            buffer_number: buffer_info.buffer_number,
                            number_of_channels: buffer_info.number_of_channels,
                        },
                    );
                });
        }

        Ok(())
    }
}
