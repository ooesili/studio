use crate::{
    filesystem::real::RealFS,
    metronome::Metronome,
    state,
    watch::client::{write_child_event, ClientCommand, ClientEvent},
};
use anyhow::{Context, Result};
use async_channel::{Receiver, Sender};
use async_std::{prelude::*, task};
use futures_lite::stream;
use serde::{Deserialize, Serialize};
use std::{
    ops::Range,
    time::{Duration, SystemTime},
};

type Slot = state::Slot<Option<Transport>>;

pub struct AsyncScheduler<J> {
    bpm: f64,
    job_def: Option<J>,
    receiver: Receiver<ClientCommand>,
    state: state::Container<RealFS>,
    sender: Sender<ClientCommand>,
    timing: Timing,
    transport_slot: Slot,
}

impl<J> AsyncScheduler<J>
where
    J: JobDef,
{
    pub fn new(
        state: state::Container<RealFS>,
        transport_slot: Slot,
        bpm: f64,
        job_def: J,
    ) -> Result<Self> {
        let (sender, receiver) = async_channel::unbounded();
        Ok(Self {
            bpm,
            job_def: Some(job_def),
            receiver,
            sender,
            state,
            timing: Timing {
                ahead_by: Duration::from_millis(250),
                scheduling_interval: Duration::from_millis(420),
            },
            transport_slot,
        })
    }

    pub fn ahead_by(mut self, duration: Duration) -> Self {
        self.timing.ahead_by = duration;
        self
    }

    pub fn sender(&self) -> Sender<ClientCommand> {
        self.sender.clone()
    }

    pub async fn run(mut self) -> Result<()> {
        let mut job = self
            .job_def
            .take()
            .unwrap()
            .init()
            .context("initializing job")?;
        write_child_event(ClientEvent::PlaybackReady).await?;

        enum State {
            Initialized,
            Playing {
                metro: Metronome,
                transport: Transport,
            },
            Stopped,
        }
        let mut state = State::Initialized;

        let commands = self.receiver.map(Action::Command);
        let (reset_time, timer) = timer();
        let mut stream = stream::race(commands, timer);

        loop {
            let action = stream
                .next()
                .await
                .unwrap_or(Action::Command(ClientCommand::Quit));

            state = match (state, action) {
                (State::Initialized, Action::Command(ClientCommand::Play)) => {
                    self.state
                        .load()
                        .context("loading scheduler state before beginning playback")?;
                    let transport =
                        self.transport_slot
                            .lock()
                            .take()
                            .unwrap_or_else(|| Transport {
                                // TODO: Figure out how to tie the offset duration here to the to scheduler look ahead.
                                epoch: SystemTime::now() + Duration::from_millis(420),
                                tick: 0,
                            });
                    let metro = Metronome::new(transport.epoch, self.bpm);
                    reset_time
                        .send(self.timing.delay(&transport))
                        .await
                        .unwrap();
                    State::Playing { transport, metro }
                }

                (
                    State::Playing {
                        mut transport,
                        metro,
                    },
                    Action::ScheduleNext,
                ) => {
                    let this_time = self.timing.tick_time(&transport);
                    transport.tick += 1;
                    let next_time = self.timing.tick_time(&transport);

                    match job.run(&metro, this_time..next_time) {
                        JobResult::Continue => {
                            reset_time
                                .send(self.timing.delay(&transport))
                                .await
                                .unwrap();
                        }
                        JobResult::Done => return Ok(()),
                    }
                    State::Playing { transport, metro }
                }

                (State::Playing { transport, .. }, Action::Command(ClientCommand::Stop)) => {
                    *self.transport_slot.lock() = Some(transport);
                    self.state
                        .save()
                        .context("saving scheduler state after receiving Stop command")?;
                    if let Err(err) = write_child_event(ClientEvent::StopConfirmed)
                        .await
                        .context("sending StopConfirmed event")
                    {
                        log::error!("sending StopConfirmed from scheduler to CLI: {}", err);
                    }
                    State::Stopped
                }

                (state, Action::Command(ClientCommand::Quit)) => {
                    if let State::Playing { .. } = state {
                        // TODO: this should be unnecessary if we reset on shutdown
                        self.state
                            .save()
                            .context("saving scheduler state after receiving Quit command")?;
                    }
                    return job.shutdown().context("shutting down job");
                }

                (state, action) => {
                    log::warn!("received unexpected action, doing nothing: {:?}", action);
                    state
                }
            }
        }
    }
}

struct Timing {
    ahead_by: Duration,
    scheduling_interval: Duration,
}

impl Timing {
    fn delay(&self, transport: &Transport) -> Duration {
        self.tick_time(transport)
            .duration_since(SystemTime::now())
            .expect("Next tick time in the past! The system clock might have moved back in time.")
            .checked_sub(self.ahead_by)
            .expect("the next job is too soon to schedule")
    }

    fn tick_time(&self, transport: &Transport) -> SystemTime {
        transport.epoch + self.scheduling_interval * transport.tick
    }
}

pub trait JobDef: Send + 'static {
    type Job: Job;
    fn init(self) -> Result<Self::Job>;
}

pub trait Job: Send + 'static {
    fn run(&mut self, metro: &Metronome, interval: Range<SystemTime>) -> JobResult;
    fn shutdown(self) -> Result<()>;
}

#[derive(Debug)]
pub enum JobResult {
    Continue,
    Done,
}

#[derive(Debug)]
enum Action {
    ScheduleNext,
    Command(ClientCommand),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Transport {
    pub epoch: SystemTime,
    pub tick: u32,
}

fn timer() -> (Sender<Duration>, impl Stream<Item = Action>) {
    use futures_lite::stream::StreamExt;

    let (time_sender, time_receiver) = async_channel::bounded(1);
    (
        time_sender,
        time_receiver
            .then(|duration| async move {
                task::sleep(duration).await;
                Action::ScheduleNext
            })
            .boxed(),
    )
}
