use std::time::Duration;
use std::time::SystemTime;

#[derive(Debug)]
pub struct Metronome {
    pub beat_duration: Duration,
    pub epoch: SystemTime,
}

impl Metronome {
    pub fn new(epoch: SystemTime, bpm: f64) -> Self {
        Metronome {
            beat_duration: Duration::from_secs(60).div_f64(bpm),
            epoch,
        }
    }

    pub fn beat_time(&self, beat: f64) -> SystemTime {
        self.epoch
            .checked_add(self.beat_duration.mul_f64(beat))
            .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_60_bpm() {
        let epoch = SystemTime::now();
        let bpm = 60.0;
        let metro = Metronome::new(epoch, bpm);
        assert_eq!(epoch, metro.beat_time(0.0));
        assert_eq!(epoch + Duration::from_secs(1), metro.beat_time(1.0));
        assert_eq!(epoch + Duration::from_secs(60), metro.beat_time(60.0));
    }

    #[test]
    fn test_120_bpm() {
        let epoch = SystemTime::now();
        let bpm = 120.0;
        let metro = Metronome::new(epoch, bpm);
        assert_eq!(epoch, metro.beat_time(0.0));
        assert_eq!(epoch + Duration::from_secs_f64(0.5), metro.beat_time(1.0));
        assert_eq!(epoch + Duration::from_secs(60), metro.beat_time(120.0));
    }
}
