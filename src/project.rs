use crate::app::TimedEvent;
use crate::filesystem::real::RealFS;
use crate::idpool::IDPool;
use crate::resource_managers::SynthDefManager;
use crate::state;
use anyhow::{Context, Result};
use sorceress::{
    pattern::{self, EventOrRest, Pattern},
    server::Server,
    synthdef::{encoder::encode_synth_defs, SynthDef},
};
use std::{
    path::PathBuf,
    time::{Duration, SystemTime},
};

pub mod drum;
pub mod tracker;

use drum::Machine;
use tracker::{LoadedTracker, Tracker};

#[derive(Debug)]
pub struct Project<E> {
    // TODO: hide modules for these sub components from outside the crate
    drum_machine: Machine,
    synthdefs: Vec<SynthDef>,
    tracker: Tracker<E>,
}

impl<E> Project<E>
where
    E: Clone,
{
    pub fn new() -> Self {
        let drum_machine = drum::Machine::new();
        let tracker = Tracker::new();
        Self {
            drum_machine,
            synthdefs: Vec::new(),
            tracker,
        }
    }

    pub fn add_synthdef(&mut self, synthdef: SynthDef) {
        self.synthdefs.push(synthdef);
    }

    pub fn add_kit(&mut self, kit: drum::Kit) {
        self.drum_machine.add_kit(kit);
    }

    pub fn add_track(&mut self, track: Track<E>) {
        self.tracker.add_track(track);
    }

    pub fn load(
        self,
        jack_latency: Duration,
        region_directory: PathBuf,
        server: Server,
        mut project_state: state::Builder<RealFS>,
        synth_ids: IDPool,
    ) -> Result<LoadedProject<E>> {
        let buffer_ids = IDPool::new(project_state.slot("idpool/buffer"), 0);
        let drum_machine_slot = project_state.slot("drum-machine");
        let synthdef_manager_slot = project_state.slot("synthdef-manager");

        let mut project_state = project_state.build();
        project_state
            .load()
            .context("loading project state before playback")?;

        let drum_machine = self
            .drum_machine
            .load(server.clone(), drum_machine_slot, buffer_ids.clone())
            .context("loading drum machine")?;

        let synthdefs = self
            .synthdefs
            .into_iter()
            .chain(drum_machine.synthdefs())
            .chain(self.tracker.synthdefs())
            .map(|synthdef| {
                (
                    synthdef.name().to_owned(),
                    encode_synth_defs(vec![synthdef]),
                )
            })
            .collect();

        SynthDefManager::new(synthdef_manager_slot, server.clone())
            .sync(&synthdefs)
            .context("synchronizing synthdefs")?;

        let tracker = self
            .tracker
            .load(
                jack_latency,
                region_directory,
                server,
                buffer_ids,
                synth_ids,
            )
            .context("loading tracker")?;

        project_state
            .save()
            .context("saving project state before beginning playback")?;

        Ok(LoadedProject {
            drum_machine,
            project_state,
            tracker,
        })
    }
}

pub struct LoadedProject<E> {
    drum_machine: drum::LoadedMachine,
    project_state: state::Container<RealFS>,
    tracker: LoadedTracker<E>,
}

impl<E> LoadedProject<E>
where
    E: Clone,
{
    pub fn handle_event(&self, time: SystemTime, msg: EventOrRest<Event>) {
        let result = match msg {
            EventOrRest::Event(Event::Drum(drum_event)) => self
                .drum_machine
                .send(TimedEvent {
                    time,
                    data: drum_event,
                })
                .context("sending event to drum machine"),
            EventOrRest::Event(Event::Tracker(tracker_event)) => self
                .tracker
                .send(TimedEvent {
                    time,
                    data: tracker_event,
                })
                .context("sending event to tracker"),
            EventOrRest::Rest => Ok(()),
        };
        if let Err(err) = result {
            log::error!("processing event: {}", err);
        }
    }

    pub fn pattern(&self) -> Pattern<E> {
        self.tracker.pattern()
    }

    pub fn shutdown(self) -> Result<()> {
        let LoadedProject {
            drum_machine,
            mut project_state,
            tracker,
        } = self;

        project_state.load().context(
            "loading project state during project shutdown to remove unneeded resources",
        )?;

        drum_machine
            .shutdown()
            .context("shutting down drum machine")?;
        drop(tracker); // TODO shutdown tracker instead of dropping it

        project_state
            .save()
            .context("saving project state during project shutdown to remove unneeded resources")?;

        // synthdefs
        // tracker
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Track<E> {
    pub channels: tracker::Channels,
    pub name: String,
    pub playlist: Playlist<E>,
    pub record: bool,
}

#[derive(Debug, Clone)]
pub enum Event {
    Drum(drum::Event),
    Tracker(tracker::Event),
}

impl From<tracker::Event> for Event {
    fn from(event: tracker::Event) -> Event {
        Event::Tracker(event)
    }
}

impl From<drum::Event> for Event {
    fn from(event: drum::Event) -> Event {
        Event::Drum(event)
    }
}

impl<E> Track<E> {
    pub fn pattern(tracks: Vec<Track<E>>) -> Pattern<E> {
        pattern::parallel(|p| {
            for track in tracks {
                for region in track.playlist.regions {
                    p.sequence(|s| {
                        s.rest(region.position);
                        s.embed(region.pattern);
                    })
                }
            }
        })
    }
}

#[derive(Debug, Clone)]
pub struct Playlist<E> {
    pub regions: Vec<Region<E>>,
}

#[derive(Debug, Clone)]
pub struct Region<E> {
    pub pattern: Pattern<E>,
    pub position: f64,
}
