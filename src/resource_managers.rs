use crate::state;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use sorceress::server::{self, Server};
use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
    ops::Deref,
};

type SynthDefs = HashMap<String, Vec<u8>>;

type Slot = state::Slot<SynthDefStateDTO>;

pub struct SynthDefManager {
    slot: Slot,
    server: Server,
}

impl SynthDefManager {
    pub fn new(slot: Slot, server: Server) -> Self {
        Self { slot, server }
    }

    pub fn sync(self, synthdefs: &SynthDefs) -> Result<()> {
        let mut state = self.load_state()?;
        self.remove_old_synthdefs(&mut state, synthdefs)?;
        self.add_new_synthdefs(&mut state, synthdefs)?;
        self.save_state(&state)?;
        Ok(())
    }

    fn load_state(&self) -> Result<SynthDefState> {
        Ok(self.slot.lock().deref().try_into()?)
    }

    fn save_state(&self, state: &SynthDefState) -> Result<()> {
        *self.slot.lock() = state.into();
        Ok(())
    }

    fn add_new_synthdefs(
        &self,
        state: &mut SynthDefState,
        desired: &HashMap<String, Vec<u8>>,
    ) -> Result<()> {
        for (name, synth_def) in desired.iter() {
            if state.synthdefs.get(name) != Some(synth_def) {
                self.server.send(server::SynthDefRecv::new(&synth_def))?;
                state.synthdefs.insert(name.clone(), synth_def.clone());
            }
        }
        if !desired.is_empty() {
            self.server.sync()?;
        }
        Ok(())
    }

    fn remove_old_synthdefs(
        &self,
        state: &mut SynthDefState,
        desired: &HashMap<String, Vec<u8>>,
    ) -> Result<()> {
        let names = desired
            .keys()
            .cloned()
            .filter(|synthdef_name| !desired.contains_key(synthdef_name))
            .collect::<Vec<_>>();
        for name in names.iter() {
            state.synthdefs.remove(name);
        }
        if names.len() > 0 {
            self.server.send(server::SynthDefFree::new(names))?;
        }
        Ok(())
    }
}

#[derive(Debug)]
struct SynthDefState {
    synthdefs: HashMap<String, Vec<u8>>,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct SynthDefStateDTO {
    synthdefs: Vec<SynthDefDTO>,
}

#[derive(Debug, Serialize, Deserialize)]
struct SynthDefDTO {
    name: String,
    synthdef: String,
}

impl From<&SynthDefState> for SynthDefStateDTO {
    fn from(server_state: &SynthDefState) -> Self {
        SynthDefStateDTO {
            synthdefs: server_state
                .synthdefs
                .iter()
                .map(|(name, synthdef)| SynthDefDTO {
                    name: name.clone(),
                    synthdef: base64::encode(synthdef),
                })
                .collect(),
        }
    }
}

impl TryFrom<&SynthDefStateDTO> for SynthDefState {
    type Error = base64::DecodeError;

    fn try_from(state_dto: &SynthDefStateDTO) -> std::result::Result<Self, Self::Error> {
        Ok(SynthDefState {
            synthdefs: state_dto
                .synthdefs
                .iter()
                .map(|SynthDefDTO { name, synthdef }| Ok((name.clone(), base64::decode(synthdef)?)))
                .collect::<std::result::Result<_, _>>()?,
        })
    }
}
