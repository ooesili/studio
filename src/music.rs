pub fn midicps(note: u32) -> f32 {
    let exp = ((note as f32) - 69.0) / 12.0;
    440.0 * 2f32.powf(exp)
}
