//! An extremely simple abstraction over a filesystem.

use std::io::Result;
use std::path::Path;

/// An extremely simple abstraction over a filesystem.
pub trait Filesystem: Clone {
    fn read<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>>;
    fn write<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, contents: C) -> Result<()>;
}

pub mod memory {
    use super::Filesystem;
    use std::cell::RefCell;
    use std::collections::HashMap;
    use std::io::{Error, ErrorKind, Result};
    use std::path::{Path, PathBuf};
    use std::rc::Rc;

    #[derive(Debug, Clone, Default)]
    pub struct MemoryFS {
        files: Rc<RefCell<HashMap<PathBuf, Vec<u8>>>>,
    }

    impl Filesystem for MemoryFS {
        fn read<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>> {
            self.files
                .borrow()
                .get(&path.as_ref().to_owned())
                .map(Clone::clone)
                .ok_or_else(|| {
                    Error::new(
                        ErrorKind::NotFound,
                        format!("file not found: {}", path.as_ref().display()),
                    )
                })
        }

        fn write<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, contents: C) -> Result<()> {
            self.files
                .borrow_mut()
                .insert(path.as_ref().to_owned(), contents.as_ref().to_vec());
            Ok(())
        }
    }
}

pub mod real {
    use super::Filesystem;
    use std::io::Result;
    use std::path::Path;

    #[derive(Debug, Clone)]
    pub struct RealFS;

    impl Filesystem for RealFS {
        fn read<P: AsRef<Path>>(&self, path: P) -> Result<Vec<u8>> {
            std::fs::read(path)
        }

        fn write<P: AsRef<Path>, C: AsRef<[u8]>>(&self, path: P, contents: C) -> Result<()> {
            std::fs::write(path, contents)
        }
    }
}
