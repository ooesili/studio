use crate::scheduler::{AsyncScheduler, JobDef};
use crate::watch::client::{pump_client_commands, pump_signals};
use anyhow::Result;
use signal_hook::consts::{SIGINT, SIGTERM};
use signal_hook_async_std::Signals;
use std::time::SystemTime;

/// A message with a SytemTime timestamp attached.
#[derive(Debug, Clone)]
pub struct TimedEvent<M> {
    pub time: SystemTime,
    pub data: M,
}

impl<M> TimedEvent<M> {
    pub fn map<F, B>(self, f: F) -> TimedEvent<B>
    where
        F: FnOnce(M) -> B,
    {
        TimedEvent {
            time: self.time,
            data: f(self.data),
        }
    }
}

pub async fn play_project<J>(scheduler: AsyncScheduler<J>) -> Result<()>
where
    J: JobDef,
{
    let signals = Signals::new(&[SIGINT, SIGTERM])?;
    pump_signals(signals, scheduler.sender());
    pump_client_commands(scheduler.sender());
    scheduler.run().await?;
    Ok(())
}
