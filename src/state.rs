//! Simple shared mutable persistent state.
//!
//! This module provides shared mutable persistent state. State is stored in a `Container` which is
//! backed by a file on the local filesystem. Each `Container` is split into multiple `Slot`
//! values. Each `Slot` is identified by a `String` key and can be shared, read from, and written
//! to independently of each other. Writes to a `Slot` are only persisted once the `Container` they
//! belong to is saved using `Container::save`.
//!
//! # Examples
//!
//! ```
//! use anyhow::Result;
//! use scrs::state::{Slot, Builder};
//! use scrs::filesystem::memory::MemoryFS;
//!
//! fn main() -> Result<()> {
//!     let fs = MemoryFS::default();
//!
//!     let mut shared_state = Builder::new(fs.clone(), "file.json".into());
//!     let sx: Slot<i32> = shared_state.slot("x");
//!     let sy: Slot<i32> = shared_state.slot("y");
//!     let mut shared_state = shared_state.build();
//!     shared_state.load()?;
//!
//!     let mut x = sx.lock();
//!     let mut y = sy.lock();
//!
//!     assert_eq!(0, *x);
//!     assert_eq!(0, *y);
//!
//!     *x = 1;
//!     *y = 2;
//!
//!     drop(x);
//!     drop(y);
//!     shared_state.save()?;
//!
//!     let mut shared_state = Builder::new(fs, "file.json".into());
//!     let sx: Slot<i32> = shared_state.slot("x");
//!     let sy: Slot<i32> = shared_state.slot("y");
//!     let mut shared_state = shared_state.build();
//!     shared_state.load()?;
//!
//!     let x = sx.lock();
//!     let y = sy.lock();
//!
//!     assert_eq!(1, *x);
//!     assert_eq!(2, *y);
//!     Ok(())
//! }
//!
//! ```

use crate::filesystem::Filesystem;
use anyhow::{Context, Result};
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::{
    collections::HashMap,
    fmt, io,
    ops::Deref,
    path::PathBuf,
    sync::{Arc, Mutex, MutexGuard},
};

/// A factory for state containers. This builder allows multiple state "Slots" to be created.
pub struct Builder<FS>
where
    FS: Filesystem,
{
    filename: PathBuf,
    fs: FS,
    state: HashMap<String, Box<dyn State + Send + 'static>>,
}

impl<FS> Builder<FS>
where
    FS: Filesystem,
{
    /// Creates a new Builder that uses the given filename and filesystem for peristence.
    pub fn new(fs: FS, filename: PathBuf) -> Self {
        Self {
            filename,
            fs,
            state: Default::default(),
        }
    }

    /// Obtain a `Slot` identified by the given key.
    pub fn slot<T>(&mut self, key: impl Into<String>) -> Slot<T>
    where
        T: fmt::Debug + Default + Serialize + DeserializeOwned + Send + 'static,
    {
        let state = Arc::new(Mutex::new(T::default()));
        self.state.insert(key.into(), Box::new(state.clone()));
        Slot { state }
    }

    pub fn build(self) -> Container<FS> {
        Container {
            filename: self.filename,
            fs: self.fs,
            state: self.state,
        }
    }
}

/// A mutable state container backed by a file.
#[derive(Debug)]
pub struct Container<FS>
where
    FS: Filesystem,
{
    filename: PathBuf,
    fs: FS,
    state: HashMap<String, Box<dyn State + Send + 'static>>,
}

impl<FS> Container<FS>
where
    FS: Filesystem,
{
    /// Loads the state of the Container from the file given to the `new` method. If the file does
    /// not exist the Container will remain empty. Any state currently stored in the container will
    /// be replaced.
    pub fn load(&mut self) -> Result<()> {
        match self.fs.read(&self.filename) {
            Ok(json_bytes) => {
                let mut loaded_state: HashMap<String, serde_json::Value> =
                    serde_json::from_slice(&json_bytes)?;
                for (key, value) in self.state.iter_mut() {
                    match loaded_state.remove(key) {
                        Some(loaded_value) => {
                            value
                                .load_json(loaded_value)
                                .with_context(|| format!("loading state key {}", key))?;
                        }
                        None => value.reset(),
                    }
                }
                Ok(())
            }
            Err(err) if err.kind() == io::ErrorKind::NotFound => Ok(()),
            Err(err) => Err(err.into()),
        }
    }

    /// Saves the current state of the `Container` to the filesystem.
    pub fn save(&self) -> Result<()> {
        let state = self
            .state
            .iter()
            .map(|(key, state)| {
                Ok((
                    key.clone(),
                    state
                        .as_json()
                        .with_context(|| format!("serializing state key {}", key))?,
                ))
            })
            .collect::<Result<HashMap<_, _>>>()?;

        log::debug!("flushing state to disk: {:?}", state);
        self.fs
            .write(
                &self.filename,
                serde_json::to_vec(&state).context("serializing state")?,
            )
            .context("writing state to the filesystem")?;
        Ok(())
    }
}

/// Slot provides access to an individual portion of state from a `Container`.
#[derive(Debug, Clone)]
pub struct Slot<T> {
    state: Arc<Mutex<T>>,
}

impl<T> Slot<T> {
    /// Obtain the value stored in the slot. A Mutex is used internally to allow the slot and
    /// container to be accessed from different threads. The value will not be accessible by other
    /// threads until the returned MutexGuard is dropped.
    pub fn lock(&self) -> MutexGuard<T> {
        self.state.lock().unwrap()
    }
}

trait State: fmt::Debug {
    fn as_json(&self) -> Result<serde_json::Value>;
    fn load_json(&mut self, value: serde_json::Value) -> Result<()>;
    fn reset(&mut self);
}

impl<T> State for Arc<Mutex<T>>
where
    T: fmt::Debug + Default + Serialize + DeserializeOwned,
{
    fn as_json(&self) -> Result<serde_json::Value> {
        Ok(serde_json::to_value(self.lock().unwrap().deref())?)
    }

    fn load_json(&mut self, value: serde_json::Value) -> Result<()> {
        *self.lock().unwrap() = serde_json::from_value(value)?;
        Ok(())
    }

    fn reset(&mut self) {
        *self.lock().unwrap() = T::default();
    }
}
