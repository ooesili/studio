#[macro_export]
macro_rules! bind {
    ( $event:ident { $( $key:ident: $value_stream:expr ),* } ) => {
        {
            $( let mut $key = $value_stream.into_iter(); )*

            std::iter::from_fn(move || {
                Some($event {
                    $( $key: $key.next()?, )*
                })
            })
        }
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn bind_test() {
        assert_eq!(
            bind!(TestEvent {
                field1: vec![1, 2, 3],
                field2: vec!["one".to_owned(), "two".to_owned(), "three".to_owned()]
            })
            .collect::<Vec<_>>(),
            vec![
                TestEvent {
                    field1: 1,
                    field2: "one".to_owned(),
                },
                TestEvent {
                    field1: 2,
                    field2: "two".to_owned(),
                },
                TestEvent {
                    field1: 3,
                    field2: "three".to_owned(),
                },
            ]
        )
    }

    #[derive(Debug, PartialEq, Eq)]
    struct TestEvent {
        field1: i32,
        field2: String,
    }
}
