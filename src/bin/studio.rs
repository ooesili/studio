#![forbid(unsafe_code)]

use anyhow::{anyhow, Context, Result};
use scrs::{
    music::midicps,
    project::{self, drum, tracker, Playlist, Project, Region, Track},
};
use sorceress::{
    pattern,
    synthdef::{self, Input},
    ugen,
    ugen::envelope::{Curve, Env},
};
use std::path::Path;

fn main() {
    pretty_env_logger::init();
    scrs::main::simple(init);
}

fn init() -> Result<Project<project::Event>> {
    let pattern = pattern::parallel(|p| {
        p.sequence(|s| {
            s.play(
                16.0,
                tracker::Event::TrackStarted {
                    segment: tracker::Segment {
                        track_name: "guitar-left".into(),
                        section_name: "intro".into(),
                    },
                },
            );
            s.play(
                0.0,
                tracker::Event::TrackStopped {
                    segment: tracker::Segment {
                        track_name: "guitar-left".into(),
                        section_name: "intro".into(),
                    },
                },
            );
        });

        p.sequence(|s| {
            for _ in 0..16 {
                s.play(1.0, drum::trigger("metal", "kick", 1.0))
            }
        });

        p.sequence(|s| {
            for _ in 0..4 {
                for _ in 0..5 {
                    s.play(0.75, note(midicps(55)));
                    s.play(0.75, note(midicps(56)));
                    s.play(0.75, note(midicps(63)));
                    s.play(0.75, note(midicps(62)));
                    s.play(1.0, note(midicps(70)));
                }
            }
        })
    });

    let kit_toml = "/home/ooesili/studio/drums/samples/metal-kit.toml";
    let mut kit: drum::Kit = toml::from_slice(
        &std::fs::read(kit_toml)
            .with_context(|| format!("opening drum kit toml file: {}", kit_toml))?,
    )
    .with_context(|| format!("parsing file {} as toml", kit_toml))?;
    let sample_dir = Path::new(kit_toml)
        .parent()
        .ok_or_else(|| anyhow!("kit toml file does not have a parent directory"))?;
    kit.set_sample_dir(sample_dir);

    let mut project = Project::new();

    project.add_synthdef(sample_synthdef());
    project.add_kit(kit);
    project.add_track(Track {
        channels: tracker::Channels::Stereo(2, 3),
        name: "guitar-left".to_owned(),
        playlist: Playlist {
            regions: vec![Region {
                pattern,
                position: 0.0,
            }],
        },
        record: false,
    });

    Ok(project)
}

fn sample_synthdef() -> synthdef::SynthDef {
    synthdef::SynthDef::new("hello", |params| {
        let freq = params.named("freq", 440.0);
        ugen::OffsetOut::ar().channels(
            ugen::SinOsc::ar().freq(vec![freq, freq]).mul(
                ugen::EnvGen::ar()
                    .envelope(
                        Env::default()
                            .levels(vec![0.0, 0.8, 0.5, 0.0])
                            .times(vec![0.01, 0.3, 1.0])
                            .curve(vec![
                                Curve::Curve((-4.0).into_value()),
                                Curve::Curve((-4.0).into_value()),
                                Curve::Curve((-4.0).into_value()),
                            ]),
                    )
                    .done_action(synthdef::DoneAction::FreeSelf),
            ),
        )
    })
}

fn note(freq: f32) -> tracker::Event {
    tracker::Event::PlaySynth {
        synth_def_name: "hello".into(),
        args: vec![("freq".into(), freq)],
    }
}
