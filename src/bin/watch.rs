use anyhow::{Context, Error, Result};
use async_std::{io, prelude::*, task};
use scrs::watch::{
    client::{write_child_event, ClientCommand, ClientEvent},
    engine,
    fs::pump_notifications,
    process_manager::{Command, ProcessManager},
    signal::pump_signals,
};
use signal_hook::consts::{SIGINT, SIGTERM};
use signal_hook_async_std::Signals;
use std::{env, path::PathBuf};

#[async_std::main]
async fn main() -> Result<()> {
    pretty_env_logger::init_timed();

    match env::var("IS_CHILD") {
        Ok(is_child) if is_child == "true" => run_child().await,
        _ => run_parent().await,
    }
}

async fn run_parent() -> Result<()> {
    let mut reactor = engine::reactor();

    let notify_args = watchexec::ArgsBuilder::default()
        .cmd(["false".to_owned()])
        .paths([PathBuf::from(".")])
        .build()
        .map_err(Error::msg)
        .context("creating filesystem watcher")?;
    pump_notifications(notify_args, reactor.sender());

    let signals = Signals::new(&[SIGINT, SIGTERM])?;
    pump_signals(signals, reactor.sender());

    let cmd = Command::new("cargo").arg("run");
    let process_manager = ProcessManager::spawn(cmd, reactor.sender());
    reactor.add_action_handler(process_manager.sender());

    reactor.run().await;
    process_manager.wait().await;

    return Ok(());
}

async fn run_child() -> Result<()> {
    let process_id = env::var("PROCESS_ID").context("reading PROCESS_ID from environment")?;

    let event = serde_json::to_string(&ClientEvent::PlaybackReady).unwrap();
    writeln!(io::stdout(), "{}", event)
        .await
        .context("writing PlaybackReady event to stdout")?;

    let mut signals = Signals::new(&[SIGINT, SIGTERM])?;
    task::spawn(async move { while let Some(_) = signals.next().await {} });

    let mut lines = io::BufReader::new(io::stdin()).lines();

    while let Some(line) = lines.next().await {
        let command = match line
            .context("reading next line")
            .and_then(|line| serde_json::from_str(&line).context("deserializing command from JSON"))
        {
            Ok(command) => command,
            Err(err) => {
                log::error!("reading next command from stdin: {}", err);
                continue;
            }
        };

        match command {
            ClientCommand::Play { .. } => {
                log::info!("child {} is now playing", process_id);
            }
            ClientCommand::Quit => {
                log::info!("child {} is quitting", process_id);
                break;
            }
            ClientCommand::Stop => {
                log::info!("child {} is now stopped", process_id);
                write_child_event(ClientEvent::StopConfirmed).await?;
            }
        }
    }

    Ok(())
}
