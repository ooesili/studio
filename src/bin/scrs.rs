use anyhow::{Context, Error, Result};
use clap::{App, AppSettings, Arg, SubCommand};
use scrs::{
    main::{Action, Command, Config},
    watch::{
        engine,
        fs::pump_notifications,
        process_manager::{self, ProcessManager},
        signal::pump_signals,
    },
};
use signal_hook::consts::{SIGINT, SIGTERM};
use signal_hook_async_std::Signals;
use sorceress::synthdef;
use std::{fs, path::PathBuf, time::Duration};

#[async_std::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();

    let app = App::new("scrs")
        .version("0.1.0")
        .about("A Rust-based music making environment")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(SubCommand::with_name("play").about("play the project"))
        .subcommand(SubCommand::with_name("loop").about("loop a segment"))
        .subcommand(
            SubCommand::with_name("demo")
                .about("demo a synth definition")
                .arg(Arg::with_name("SYNTHDEF_NAME").index(1).required(true)),
        )
        .subcommand(SubCommand::with_name("record").about("record over a segment"))
        .subcommand(SubCommand::with_name("reset").about("reset server state"))
        .subcommand(
            SubCommand::with_name("print-synthdef")
                .about("pretty print a synthdef file")
                .arg(Arg::with_name("FILE").index(1).required(true)),
        );

    let matches = app.get_matches();

    match matches.subcommand() {
        ("play", Some(_command)) => unimplemented!(),
        ("record", Some(_command)) => unimplemented!(),
        ("loop", Some(_command)) => cmd_loop().await,
        ("demo", Some(command)) => {
            cmd_demo(command.value_of("SYNTHDEF_NAME").unwrap().to_owned()).await
        }
        ("reset", Some(_command)) => cmd_reset(),
        ("print-synthdef", Some(command)) => cmd_print_synthdef(command.value_of("FILE").unwrap()),
        (name, _) => unreachable!("unknown command: {}", name),
    }
}

async fn cmd_loop() -> Result<()> {
    run_engine(Action::Loop).await
}

async fn cmd_demo(synthdef_name: String) -> Result<()> {
    run_engine(Action::Demo { synthdef_name }).await
}

async fn run_engine(action: Action) -> Result<()> {
    let command = serde_json::to_string(&Command {
        action,
        config: new_config().context("creating configuration")?,
    })
    .context("encoding initial command to send to client as JSON")?;

    let mut reactor = engine::reactor();
    let notify_args = watchexec::ArgsBuilder::default()
        .cmd(["false".to_owned()])
        .paths(["src".into()])
        .build()
        .map_err(Error::msg)
        .context("creating filesystem watcher")?;

    pump_notifications(notify_args, reactor.sender());

    let signals = Signals::new(&[SIGINT, SIGTERM])?;
    pump_signals(signals, reactor.sender());

    let cmd = process_manager::Command::new("cargo")
        .args(&["run", "--bin", "studio"])
        .env(
            "SORCERESS_MAGIC_TOKEN".to_owned(),
            "7is1rzohsEbtujaufKVwfs".to_owned(),
        )
        .env("SORCERESS_COMMAND".to_owned(), command);
    let process_manager = ProcessManager::spawn(cmd, reactor.sender());
    reactor.add_action_handler(process_manager.sender());

    reactor.run().await;
    process_manager.wait().await;

    Ok(())
}

fn cmd_reset() -> Result<()> {
    let config = new_config()?;

    // We don't care if calls these fail so we can ignore their errors.
    let _ = fs::remove_file(config.project_state_file);
    let _ = fs::remove_file(config.scheduler_state_file);

    Ok(())
}

fn cmd_print_synthdef(filename: impl AsRef<std::path::Path>) -> Result<()> {
    let file = std::fs::File::open(filename).context("opening file")?;
    synthdef::decoder::SynthDefFile::decode(file).context("parsing file")?;
    Ok(())
}

fn new_config() -> Result<Config> {
    let jack_latency = {
        let jack_latency_ms = std::env::var("JACK_LATENCY_MS")
            .ok()
            .and_then(|ms| ms.parse::<u64>().ok())
            .unwrap_or(0);
        Duration::from_millis(jack_latency_ms)
    };

    let xdg_directories = xdg::BaseDirectories::new().context("looking up XDG directories")?;
    let data_directory = xdg_directories.get_data_home().join("sorceress");
    std::fs::create_dir_all(&data_directory).context("creating data directory")?;
    let region_directory = xdg_directories
        .place_data_file(PathBuf::from("sorceress").join("regions"))
        .context("placing regions data directory")?;

    Ok(Config {
        jack_latency,
        project_state_file: data_directory.join("project-state.json"),
        region_directory,
        scheduler_state_file: data_directory.join("scheduler-state.json"),
        server_address: "127.0.0.1:57110".to_owned(),
    })
}
