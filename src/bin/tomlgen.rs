use scrs::project::drum::Kit;
use std::path::Path;

fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    if false {
        toml_fragemnt_instrument(7, 6, "high-tom", |x, y| {
            format!(
                "tmkd/TMKD_S2_RackTom_10_maple/WAV/TMKD_S2_tom10_maple_L{}_{:02}.wav",
                x, y
            )
        });
        return Ok(());
    }

    let manifest = "/home/ooesili/studio/drums/samples/metal-kit.toml";
    let mut kit: Kit = toml::from_slice(&std::fs::read(manifest)?)?;

    let samples_dir = Path::new(manifest).parent().unwrap();
    kit.set_sample_dir(samples_dir);

    println!("{:#?}", kit);
    Ok(())
}

fn toml_fragemnt_instrument(
    number_of_layers: i32,
    samples_per_layer: i32,
    instrument: &str,
    path: impl Fn(i32, i32) -> String,
) {
    println!(
        "
[instrument.{}]
pan = 0\
                ",
        instrument
    );

    for i in 1..=number_of_layers {
        let velocity = i as f32 / number_of_layers as f32;

        println!(
            "
  [[instruments.{}.layers]]
  velocity = {}\
                ",
            instrument, velocity,
        );

        for j in 1..=samples_per_layer {
            println!(
                "
    [[instruments.{}.layers.samples]]
    path = \"{}\"\
                ",
                instrument,
                path(i, j),
            );
        }
    }
}
