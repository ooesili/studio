#![forbid(unsafe_code)]

mod macros;

pub mod app;
pub mod filesystem;
pub mod idpool;
#[path = "main_mod.rs"]
pub mod main;
pub mod metronome;
pub mod music;
pub mod project;
pub mod resource_managers;
pub mod scheduler;
pub mod state;
pub mod watch;

// TODO rename synth_ids to node_ids

// These are orphaned functions from the server module.
//
// pub fn buffer_query(&self, buffer_numbers: Vec<i32>) -> Result<Vec<BufferInfo>> {
//     let command = BufferQuery::new(buffer_numbers);
//     let (sender, receiver) = mpsc::channel();
//
//     let subscriber = Subscriber::new(move |reply| match reply {
//         Reply::BufferInfo { buffers } => sender.send(buffers.clone()).unwrap(),
//         _ => {}
//     });
//
//     self.with_subscription(subscriber, || {
//         self.send(command)?;
//         Ok(receiver.recv().unwrap())
//     })
// }
//
