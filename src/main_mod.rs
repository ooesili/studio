use crate::{
    app::play_project,
    filesystem::real::RealFS,
    idpool::IDPool,
    metronome::Metronome,
    project::{self, LoadedProject, Project},
    scheduler::{AsyncScheduler, Job, JobDef, JobResult},
    state,
    watch::client::{
        pump_client_commands, pump_signals, write_child_event, ClientCommand, ClientEvent,
    },
};
use anyhow::{bail, Context, Result};
use async_std::task;
use serde::{Deserialize, Serialize};
use signal_hook::consts::{SIGINT, SIGTERM};
use signal_hook_async_std::Signals;
use sorceress::server::{self, Reply, Server};
use std::{
    env,
    ops::Range,
    path::PathBuf,
    thread,
    time::{Duration, SystemTime},
};

pub fn simple<F>(init: F)
where
    F: FnOnce() -> Result<Project<project::Event>>,
{
    if let Err(err) = try_simple(init) {
        log::error!("fatal error: {:?}", err);
        std::process::exit(1);
    }
}

fn try_simple<F>(init: F) -> Result<()>
where
    F: FnOnce() -> Result<Project<project::Event>>,
{
    let command =
        Command::read_from_env().context("you must use the `scrs` command to run this program")?;

    match command.action {
        Action::Loop => play(command.config, EndAction::Repeat, init),
        Action::PlayOnce => play(command.config, EndAction::Stop, init),
        Action::Demo { synthdef_name } => demo(command.config, synthdef_name, init),
        Action::Record => unimplemented!(),
    }
}

fn play<F>(config: Config, end_action: EndAction, init: F) -> Result<()>
where
    F: FnOnce() -> Result<Project<project::Event>>,
{
    let Config {
        jack_latency,
        project_state_file,
        region_directory,
        scheduler_state_file,
        server_address,
    } = config;

    let server = Server::connect(&server_address).context("creating server")?;

    let replies = server.subscribe();
    thread::spawn(move || {
        for reply in replies {
            if let Reply::Fail { command, error, .. } = reply {
                log::error!("command {} failed: {}", command, error);
            }
        }
    });

    let project_state = state::Builder::new(RealFS, project_state_file);
    let mut scheduler_state = state::Builder::new(RealFS, scheduler_state_file);

    let synth_ids = IDPool::new(scheduler_state.slot("idpool/synth"), 1000);
    let transport_slot = scheduler_state.slot("transport");

    let job_def = PlayerJobDef {
        end_action,
        jack_latency,
        project: init().context("initializing project")?,
        project_state,
        region_directory,
        server,
        synth_ids,
    };

    let bpm = 120.0;
    let scheduler = AsyncScheduler::new(scheduler_state.build(), transport_slot, bpm, job_def)?;

    task::block_on(play_project(scheduler))
}

fn demo<F>(config: Config, synthdef_name: String, init: F) -> Result<()>
where
    F: FnOnce() -> Result<Project<project::Event>>,
{
    let Config {
        jack_latency,
        project_state_file,
        region_directory,
        scheduler_state_file,
        server_address,
    } = config;

    let server =
        Server::connect(server_address).context("connecting to the SuperCollider server")?;

    let project_state = state::Builder::new(RealFS, project_state_file);
    let mut scheduler_state = state::Builder::new(RealFS, scheduler_state_file);

    let (sender, receiver) = async_channel::unbounded();
    let signals = Signals::new(&[SIGINT, SIGTERM])?;
    pump_signals(signals, sender.clone());
    pump_client_commands(sender);

    let synth_ids = IDPool::new(scheduler_state.slot("idpool/synth"), 1000);
    let project = init().context("initializing project")?;

    let project = project
        .load(
            jack_latency,
            region_directory,
            server.clone(),
            project_state,
            synth_ids,
        )
        .context("loading project")?;

    task::block_on(async {
        enum State {
            Ready,
            Playing,
            Stopped,
        }

        write_child_event(ClientEvent::PlaybackReady).await?;
        let mut state = State::Ready;

        loop {
            state = match (state, receiver.recv().await.unwrap()) {
                (State::Ready, ClientCommand::Play) => {
                    server
                        .send(server::SynthNew::new(synthdef_name.clone(), 0))
                        .context("sending SynthNew command")?;
                    State::Playing
                }
                (State::Ready, ClientCommand::Stop) => {
                    write_child_event(ClientEvent::StopConfirmed).await?;
                    State::Stopped
                }
                (State::Playing, ClientCommand::Stop) => {
                    server.reset()?;
                    write_child_event(ClientEvent::StopConfirmed).await?;
                    State::Stopped
                }
                (_, ClientCommand::Quit) => return project.shutdown(),
                (state, _) => state,
            }
        }
    })
}

enum EndAction {
    Stop,
    Repeat,
}

struct PlayerJobDef {
    end_action: EndAction,
    jack_latency: Duration,
    project: Project<project::Event>,
    project_state: state::Builder<RealFS>,
    region_directory: PathBuf,
    server: Server,
    synth_ids: IDPool,
}

impl JobDef for PlayerJobDef {
    type Job = Player;

    fn init(self) -> Result<Self::Job> {
        Ok(Player {
            project: self
                .project
                .load(
                    self.jack_latency,
                    self.region_directory,
                    self.server,
                    self.project_state,
                    self.synth_ids,
                )
                .context("loading project")?,
            end_action: self.end_action,
        })
    }
}

struct Player {
    end_action: EndAction,
    project: LoadedProject<project::Event>,
}

impl Job for Player {
    fn run(&mut self, metro: &Metronome, interval: Range<SystemTime>) -> JobResult {
        match self.end_action {
            EndAction::Repeat => self.play_loop(metro, interval),
            EndAction::Stop => self.play_interval(metro, interval),
        }
    }

    fn shutdown(self) -> Result<()> {
        self.project.shutdown()
    }
}

impl Player {
    fn play_interval(&self, metro: &Metronome, interval: Range<SystemTime>) -> JobResult {
        let pattern = self.project.pattern();
        let mut seeked_stream = pattern
            .into_iter()
            .map(|event| (metro.beat_duration.mul_f64(event.delta), event.event))
            .scan(metro.epoch, |time, (duration, event_data)| {
                let event_time = *time;
                *time = time.checked_add(duration).unwrap();
                Some((event_time, event_data))
            })
            .skip_while(|(time, _)| *time < interval.start)
            .peekable();

        if seeked_stream.peek().is_none() {
            return JobResult::Done;
        }

        seeked_stream
            .take_while(|(time, _)| *time < interval.end)
            .for_each(|(time, msg)| self.project.handle_event(time, msg));

        JobResult::Continue
    }

    fn play_loop(&self, metro: &Metronome, interval: Range<SystemTime>) -> JobResult {
        let pattern = self.project.pattern();
        let events = pattern
            .into_iter()
            .map(|event| (metro.beat_duration.mul_f64(event.delta), event.event))
            .collect::<Vec<_>>();

        let pattern_duration = events
            .iter()
            .fold(Duration::default(), |duration, (event_duration, _)| {
                duration + *event_duration
            });

        let mut current_loop_start = metro.epoch;
        loop {
            let next_loop_start = current_loop_start + pattern_duration;
            if next_loop_start > interval.start {
                break;
            }
            current_loop_start = next_loop_start;
        }

        events
            .into_iter()
            .cycle()
            .scan(current_loop_start, |time, (duration, event_data)| {
                let event_time = *time;
                *time = time.checked_add(duration).unwrap();
                Some((event_time, event_data))
            })
            .skip_while(|(time, _)| *time < interval.start)
            .take_while(|(time, _)| *time < interval.end)
            .for_each(|(time, msg)| self.project.handle_event(time, msg));

        JobResult::Continue
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Command {
    pub action: Action,
    pub config: Config,
}

impl Command {
    fn read_from_env() -> Result<Self> {
        if !env::var("SORCERESS_MAGIC_TOKEN")
            .map(|token| token == "7is1rzohsEbtujaufKVwfs")
            .unwrap_or(false)
        {
            bail!("magic token not set correctly");
        }
        let json = env::var("SORCERESS_COMMAND").unwrap_or("".to_owned());
        serde_json::from_str(&json).context("parsing first process argument as JSON")
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum Action {
    Loop,
    Demo { synthdef_name: String },
    PlayOnce,
    Record,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub jack_latency: Duration,
    pub project_state_file: PathBuf,
    pub region_directory: PathBuf,
    pub scheduler_state_file: PathBuf,
    pub server_address: String,
}
