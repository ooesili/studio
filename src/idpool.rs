use crate::state;
use std::collections::HashSet;

type Slot = state::Slot<HashSet<i32>>;

#[derive(Debug, Clone)]
pub struct IDPool {
    slot: Slot,
    start: i32,
}

impl IDPool {
    pub fn new(slot: Slot, start: i32) -> Self {
        IDPool { slot, start }
    }

    pub fn next(&self) -> i32 {
        let mut ids = self.slot.lock();
        let id = (self.start..)
            .find(|id| !ids.contains(id))
            .expect("ID space exhausted");
        ids.insert(id);
        id
    }

    pub fn release(&self, id: i32) {
        self.slot.lock().remove(&id);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filesystem::memory::MemoryFS;
    use crate::state;
    use std::iter::repeat_with;

    #[test]
    fn ids_start_with_the_given_value() {
        let mut builder = memory_container();
        let slot = builder.slot("test");
        let id_pool = IDPool::new(slot, 1000);
        assert_eq!(1000, id_pool.next());
    }

    #[test]
    fn ids_do_not_repeat() {
        let mut builder = memory_container();
        let slot = builder.slot("test");
        let id_pool = IDPool::new(slot, 0);
        let ids = repeat_with(|| id_pool.next()).take(5).collect::<Vec<_>>();
        assert_eq!(vec![0, 1, 2, 3, 4], ids);
    }

    #[test]
    fn ids_can_be_released_back_to_the_pool_and_reused() {
        let mut builder = memory_container();
        let slot = builder.slot("test");
        let id_pool = IDPool::new(slot, 100);
        let ids = repeat_with(|| id_pool.next()).take(3).collect::<Vec<_>>();
        assert_eq!(vec![100, 101, 102], ids);

        id_pool.release(100);
        let ids = repeat_with(|| id_pool.next()).take(3).collect::<Vec<_>>();
        assert_eq!(vec![100, 103, 104], ids);
    }

    fn memory_container() -> state::Builder<MemoryFS> {
        let fs = MemoryFS::default();
        state::Builder::new(fs, "state.json".into())
    }
}
