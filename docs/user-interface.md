# User Interface

This document describes contains a mock terminal session with sorceress. The session displays a minimal set of features for a semi productive music making environment.

## Mockup

```
# Play a song from start to finish
$ scrs play

# Play a section with lead-in
$ scrs play --instrument=<instrument-name> --section-name=<section-name> --lead-in=<beats>

# Create a fixed length loop and begin writing drums
$ scrs loop --section-name=<section-name>

# Record section with lead-in
$ scrs record --instrument=<instrument-name> --section-name=<section-name> --lead-in=<beats>

# Reset server state (useful for when the server is restarted)
$ scrs reset

# Free all resources on the server
$ scrs shutdown
```

## Ideas for other Types of Interfaces

- mobile app
- website
- cli
- curses/termbox
- midi control surface
- input audio signal properties, e.g. pitch, volume
